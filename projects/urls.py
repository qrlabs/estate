from django.urls import path

from .import views


urlpatterns = [
    path('gallery/', views.gallery_list, name="gallery_list"),
    path('<slug>/', views.property_update_detail, name="update_detail"),
    path('', views.featured_properties_list, name="featured_properties_list"),
    path(
        '<int:id>/detail/',
        views.featured_property_detail,
        name="featured_property_detail"
    ),
]
