from django.urls import path

from .import views


urlpatterns = [
    path('list/', views.video_list, name="video_list"),
    path('<int:id>/detail/', views.video_detail, name="video_detail"),
]
