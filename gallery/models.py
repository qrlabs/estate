from django.db import models
from django.utils import timezone

from modelcluster.fields import ParentalKey

from wagtail.core.models import Page, Orderable
from wagtail.core.fields import RichTextField
from wagtail.admin.edit_handlers import FieldPanel, InlinePanel
from wagtail.images.edit_handlers import ImageChooserPanel


class StageIndexPage(Page):
    intro = RichTextField(blank=True)

    def get_context(self, request):
        context = super(StageIndexPage, self).get_context(request)
        pages = self.get_children().live().order_by('-first_published_at')
        context['stagepages'] = pages
        return context

    content_panels = Page.content_panels + [
        FieldPanel('intro', classname='full'),
    ]

    subpage_types = ['gallery.StagePage']


class StagePage(Page):
    body = RichTextField()
    date = models.DateField(default=timezone.now)
    pic = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    content_panels = Page.content_panels + [
        FieldPanel('body', classname='full'),
        FieldPanel('date'),
        ImageChooserPanel('pic'),
        InlinePanel('stage_images', label='Stage Pictures'),
    ]


class StagePageGalleryImage(Orderable):
    page = ParentalKey(StagePage, related_name='stage_images')
    image = models.ForeignKey(
        'wagtailimages.Image',
        on_delete=models.CASCADE,
        related_name='+'
    )
    caption = models.CharField(max_length=100, blank=True)
    description = models.CharField(max_length=250, blank=True)

    panels = [
        ImageChooserPanel('image'),
        FieldPanel('caption'),
        FieldPanel('description'),
    ]
