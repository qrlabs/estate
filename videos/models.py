from django.urls import reverse
from django.utils import timezone
from django.db import models

# Create your models here.


class Video(models.Model):
    title = models.CharField(max_length=120)
    embed_code = models.CharField(max_length=500, null=True, blank=True)
    description = models.TextField()
    date_uploaded = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("video_detail", kwargs={"id": self.id})

