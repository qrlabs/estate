from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render, get_object_or_404, redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from itertools import chain

# Create your views here.
from .models import Property, FeaturedProperty
from videos.models import Video


def properties_list(request):
    property_list = Property.objects.all()
    query = request.GET.get("q")
    if query:
        property_list = property_list.filter(
            Q(title__icontains=query) |
            Q(state__icontains=query) |
            Q(city__icontains=query) |
            Q(address__icontains=query) |
            Q(content__icontains=query) |
            Q(image_name__icontains=query)
            ).distinct()
    paginator = Paginator(property_list, 16)
    page_request_var = "page"
    page = request.GET.get(page_request_var)
    try:
        queryset = paginator.page(page)
    except PageNotAnInteger:
        #serve first page if page is not an integer
        queryset = paginator.page(1)
    except EmptyPage:
        queryset = paginator.page(paginator.num_pages)
    return render(
        request,
        "projects/properties_list.html",
        {
            "properties": queryset,
            "page_request_var": page_request_var
        }
    )


def property_update_detail(request, slug):
    instance = get_object_or_404(Property, slug=slug)
    updates = []

    return render(
        request,
        "projects/property_update_details.html",
        {
            "all_updates": updates,
            "instance": instance
        }
    )


def featured_properties_list(request):
    property_list = FeaturedProperty.objects.all()
    for item in property_list:
        item.price_plan = item.payment_set.all()

    query = request.GET.get("q")
    if query:
        property_list = property_list.filter(
            Q(title__icontains=query) |
            Q(state__icontains=query) |
            Q(city__icontains=query) |
            Q(address__icontains=query) |
            Q(content__icontains=query) |
            Q(image_name__icontains=query)
        ).distinct()
    paginator = Paginator(property_list, 16)
    page_request_var = "page"
    page = request.GET.get(page_request_var)
    try:
        queryset = paginator.page(page)
    except PageNotAnInteger:
        queryset = paginator.page(1)
    except EmptyPage:
        queryset = paginator.page(paginator.num_pages)
    return render(
        request,
        "projects/featured_list.html",
        {
            "properties": queryset,
            "page_request_var": page_request_var
        })


def featured_property_detail(request, id):
    instance = get_object_or_404(FeaturedProperty, id=id)
    updates = []

    return render(
        request,
        "projects/property_update_details.html",
        {
            "all_updates": updates,
            "instance": instance
        }
    )


def gallery_list(request):
    property_list = Property.objects.all()
    video_list = Video.objects.all()
    result_list = list(chain(property_list, video_list))
    query = request.GET.get("q")
    if query:
        result_list = result_list.filter(
            Q(title__icontains=query) |
            Q(state__icontains=query) |
            Q(city__icontains=query) |
            Q(address__icontains=query) |
            Q(content__icontains=query) |
            Q(image_name__icontains=query) |
            Q(embed_code__icontains=query) |
            Q(description__icontains=query)
        ).distinct()
    paginator = Paginator(result_list, 16)
    page_request_var = "page"
    page = request.GET.get(page_request_var)
    try:
        queryset = paginator.page(page)
    except PageNotAnInteger:
        queryset = paginator.page(1)
    except EmptyPage:
        queryset = paginator.page(paginator.num_pages)
    return render(
        request,
        "projects/gallery_list.html",
        {
            "property_list": property_list,
            "video_list": video_list
        }
    )
        # "properties": queryset, "page_request_var": page_request_var
